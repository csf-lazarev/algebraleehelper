from syntax.ast_tree import ExpressionNode, BinaryOperationNode, MINUS_ONE
from syntax.parser import global_parser
from syntax.types import BinOp


def sum_of(left: ExpressionNode, right: ExpressionNode):
    return BinaryOperationNode(BinOp.ADD, left, right)


def get_parsed_matrix(lee_raw):
    lee = {}

    for (l, r), v in lee_raw.items():
        value = global_parser.parse_string(v)
        lee[(l, r)] = value
        lee[(r, l)] = BinaryOperationNode(BinOp.MUL, MINUS_ONE, value)

    return lee


def create_algebra_lee_variables(count):
    # return array of e1, ..., ecount IdentifierNode
    return [global_parser.parse_string(f"e{i + 1}") for i in range(count)]


def print_commutation_check_result(info):
    for i in info:
        a, b, c = [e.name for e in i[0]]
        res = i[1]
        print(f"[[{a}, {b}], {c}] + [[{c}, {a}], {b}] + [[{b}, {c}], {a}] = {res}")


def get_algebra_lee_verdict(info):
    for _, res in info:
        if res is None:
            return False
    return True
