import re
from itertools import combinations

from py_expression_eval import Parser

from syntax.ast_tree import IdentifierNode
from syntax.visitor import LiteralCollector
from tool.common import sum_of
from tool.commutation import CommutationHelper


class YakobiLee:
    def __init__(self, lee):
        self.lee = lee

    def find(self, a: IdentifierNode, b: IdentifierNode, c: IdentifierNode):
        cmt = CommutationHelper(self.lee)
        first = cmt.commutation(cmt.commutation(a, b), c)
        second = cmt.commutation(cmt.commutation(c, a), b)
        third = cmt.commutation(cmt.commutation(b, c), a)
        return sum_of(sum_of(first, second), third)

    def check_yakobi_all(self, variables: list[IdentifierNode]):
        to_check = combinations(variables, 3)

        e_replacement = {f"xyz{i}": i + 1 for i in range(len(variables) + 1)}

        out_info = []
        parser = Parser()

        for combination in to_check:
            commutation = self.find(*combination)
            visitor = LiteralCollector()
            commutation.visit(visitor)
            literal_map = {l: 1000 + i for i, l in enumerate(visitor.literals)}
            replacement = dict(e_replacement)
            replacement.update(literal_map)
            out = re.sub(r"e(\d+)", r"xyz\1", str(commutation))
            value = parser.parse(out).simplify(replacement).toString()
            result = 0 if value == 0 else None
            out_info.append((combination, result))

        return out_info
