from syntax.ast_tree import *
from syntax.types import BinOpAddGroup, BinOpMulGroup


class CommutationResolver:

    def __init__(self, lee):
        self.lee = lee

    @abstractmethod
    def get_commutation(self):
        pass


class CommutationHelper:
    def __init__(self, lee_matrix):
        self.lee_matrix = lee_matrix

    def resolver_for(self, left: AstNode, right: AstNode) -> CommutationResolver:
        return resolver_map[(type(left), type(right))](self.lee_matrix, left, right)

    def commutation(self, left: AstNode, right: AstNode) -> ExpressionNode:
        return self.resolver_for(left, right).get_commutation()


class SimpleIdentifierCommutationResolver(CommutationResolver):

    def __init__(self, lee, left: IdentifierNode, right: IdentifierNode):
        super().__init__(lee)
        self.left = left
        self.right = right

    def get_commutation(self):
        return self.lee[(self.left.name, self.right.name)] if (self.left.name,
                                                               self.right.name) in self.lee else ZERO_LITERAL


class BinaryOperationResolverLeft(CommutationResolver):
    def get_commutation(self):
        normalize_binary_node(self.left)
        if self.left.is_literal():
            if self.left.is__zero_literal():
                return ZERO_LITERAL
            else:
                raise Exception("Non-zero literal cannot be commutation operand")
        else:
            cmt = CommutationHelper(self.lee)
            # [(a + b) * e1, e2]
            if self.left.op in BinOpAddGroup:

                if self.left.arg1.is_literal():
                    raise Exception("Expressions like 'a + e1' is not allowed")

                node_left = cmt.resolver_for(self.left.arg1, self.right).get_commutation()
                node_right = cmt.resolver_for(self.left.arg2, self.right).get_commutation()
                return BinaryOperationNode(self.left.op, node_left, node_right)

            # [a * e1 + e2]
            elif self.left.op in [BinOp.MUL]:
                if not self.left.arg1.is_literal():
                    raise Exception("Expressions like 'e1 * e2' is not allowed")

                node_left = self.left.arg1
                node_right = self.left.arg2
                commutation = cmt.resolver_for(node_right, self.right).get_commutation()
                return BinaryOperationNode(self.left.op, node_left, commutation)

    def __init__(self, lee, left: BinaryOperationNode, right: IdentifierNode):
        super().__init__(lee)
        self.left = left
        self.right = right


class BinaryOperationResolverRight(CommutationResolver):
    def get_commutation(self):
        normalize_binary_node(self.right)
        if self.right.is_literal():
            if self.right.is__zero_literal():
                return ZERO_LITERAL
            else:
                raise Exception("Non-zero literal cannot be commutation operand")
        else:
            cmt = CommutationHelper(self.lee)
            # [e1, e2 + e3]
            if self.right.op in BinOpAddGroup:

                if self.right.arg1.is_literal():
                    raise Exception("Expressions like 'a + e1' is not allowed")

                node_left = cmt.resolver_for(self.left, self.right.arg1).get_commutation()
                node_right = cmt.resolver_for(self.left, self.right.arg2).get_commutation()
                return BinaryOperationNode(self.right.op, node_left, node_right)

            # [e1, a * e2]
            elif self.right.op in BinOpMulGroup:
                if not self.right.arg1.is_literal():
                    raise Exception("Expressions like 'e1 * e2' is not allowed")

                node_left = self.right.arg1
                node_right = self.right.arg2
                commutation = cmt.resolver_for(self.left, node_right).get_commutation()
                return BinaryOperationNode(self.right.op, node_left, commutation)

    def __init__(self, lee, left: IdentifierNode, right: BinaryOperationNode):
        super().__init__(lee)
        self.left = left
        self.right = right


class BinaryOperationNodeResolver(CommutationResolver):

    def __init__(self, lee, left: BinaryOperationNode, right: BinaryOperationNode):
        super().__init__(lee)
        self.left = left
        self.right = right

    def get_commutation(self):

        if self.left.is_literal():
            if self.left.is__zero_literal():
                return ZERO_LITERAL
            else:
                raise Exception("Non-zero literal cannot be commutation operand")

        if self.right.is_literal():
            if self.right.is__zero_literal():
                return ZERO_LITERAL
            else:
                raise Exception("Non-zero literal cannot be commutation operand")

        normalize_binary_node(self.left)
        normalize_binary_node(self.right)

        # [a * e1, binary op]
        if self.left.arg1.is_literal():

            if self.left.op in [BinOp.ADD, BinOp.MUL]:
                raise Exception("Expressions like 'a + e1' is not allowed")

            cmt = CommutationHelper(self.lee).resolver_for(self.left.arg2, self.right).get_commutation()
            return BinaryOperationNode(self.left.op, self.left.arg1, cmt)
        # [a * e1 + b * e2]
        else:
            if self.left.op in BinOpMulGroup:
                raise Exception("Expressions like 'e1 * e2' is not allowed")
            cmt_left = CommutationHelper(self.lee).resolver_for(self.left.arg1, self.right).get_commutation()
            cmt_right = CommutationHelper(self.lee).resolver_for(self.left.arg2, self.right).get_commutation()
            return BinaryOperationNode(self.left.op, cmt_left, cmt_right)


class LiteralZeroAndIdentifier(CommutationResolver):

    def get_commutation(self):
        if self.literal.is__zero_literal():
            return ZERO_LITERAL
        else:
            raise Exception("Non-zero literal cannot be commutation operand")

    def __init__(self, lee, left: AstNode, right: AstNode):
        super().__init__(lee)
        if left.is_literal():
            self.literal = left
        else:
            self.literal = right


resolver_map = {
    (IdentifierNode, IdentifierNode): SimpleIdentifierCommutationResolver,
    (IdentifierNode, BinaryOperationNode): BinaryOperationResolverRight,
    (BinaryOperationNode, IdentifierNode): BinaryOperationResolverLeft,
    (BinaryOperationNode, BinaryOperationNodeResolver): BinaryOperationNodeResolver,
    (LiteralNode, IdentifierNode): LiteralZeroAndIdentifier,
    (IdentifierNode, LiteralNode): LiteralZeroAndIdentifier,
    (LiteralNode, BinaryOperationNode): LiteralZeroAndIdentifier,
    (BinaryOperationNode, LiteralNode): LiteralZeroAndIdentifier
}
