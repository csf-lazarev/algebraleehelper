from tool.common import *
from tool.yakobi import YakobiLee

if __name__ == '__main__':
    lee_raw = {
        ("e1", "e2"): "e4",
        ("e1", "e3"): "e5",
        ("e2", "e6"): "e2",
        ("e3", "e7"): "e3",
        ("e4", "e6"): "e4",
        ("e5", "e7"): "e5"
    }

    lee = get_parsed_matrix(lee_raw)
    vars = create_algebra_lee_variables(7)

    yakobi = YakobiLee(lee)
    info = yakobi.check_yakobi_all(vars)
    print_commutation_check_result(info)
    verdict = get_algebra_lee_verdict(info)
    print(f"Given Algebra Lee is {'RIGHT' if verdict else 'WRONG'}")
