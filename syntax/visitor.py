from abc import abstractmethod

from syntax.ast_tree import AstNode, LiteralNode


class AstNodeVisitor:
    @abstractmethod
    def accept(self, node: AstNode):
        pass


class LiteralCollector(AstNodeVisitor):

    def __init__(self):
        self.literals = set()

    def accept(self, node: AstNode):
        if type(node) == LiteralNode:
            literal = node.literal
            try:
                eval(literal)
            except Exception:
                self.literals.add(literal)
