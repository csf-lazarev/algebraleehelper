from abc import ABC, abstractmethod
from enum import Enum
from typing import Callable, Tuple

from syntax.types import BinOp


class AstNode(ABC):
    init_action: Callable[['AstNode'], None] = None

    def __init__(self, **props) -> None:
        super().__init__()
        self.row = None
        self.col = None
        for k, v in props.items():
            setattr(self, k, v)
        if AstNode.init_action is not None:
            AstNode.init_action(self)
        self.node_type = None
        self.node_ident = None
        self.inner_scope = None

    @property
    def childs(self) -> Tuple['AstNode', ...]:
        return ()

    def visit(self, visitor):
        visitor.accept(self)

    @abstractmethod
    def __str__(self) -> str:
        pass

    @abstractmethod
    def is_literal(self):
        return False

    @abstractmethod
    def is__zero_literal(self):
        return False

    @property
    def tree(self) -> [str, ...]:
        res = [str(self)]
        childs = self.childs
        for i, child in enumerate(childs):
            ch0, ch = '├', '│'
            if i == len(childs) - 1:
                ch0, ch = '└', ' '
            res.extend(((ch0 if j == 0 else ch) + ' ' + s for j, s in enumerate(child.tree)))
        return res

    def to_str(self):
        return str(self)

    def __getitem__(self, index):
        return self.childs[index] if index < len(self.childs) else None


class _GroupNode(AstNode):

    def __init__(self, name: str, *childs: AstNode, **props) -> None:
        super().__init__(**props)
        self.name = name
        self._childs = childs

    def __str__(self) -> str:
        return self.name

    # TODO: must be improved
    def is_literal(self):
        return self._childs[0].is_literal()

    def is__zero_literal(self):
        return self._childs[0].is__zero_literal()

    @property
    def childs(self) -> Tuple[AstNode, ...]:
        return self._childs


class ExpressionNode(AstNode, ABC):
    pass


class LiteralNode(ExpressionNode):

    def __init__(self, literal: str, **props) -> None:
        super().__init__(**props)
        self.literal = literal

        if not literal:
            self.value = None
            self.literal = None
        else:
            self.value = literal
            try:
                self.value = eval(literal)
            except Exception:
                pass

    def __str__(self) -> str:
        return f"{self.literal}"

    def is_literal(self):
        return True

    def is__zero_literal(self):
        return self.value == 0


class IdentifierNode(ExpressionNode):
    def is_literal(self):
        return False

    def is__zero_literal(self):
        return False

    def __init__(self, name: str, **props) -> None:
        super().__init__(**props)
        self.name = str(name)

    def __str__(self) -> str:
        return str(self.name)

    def __lt__(self, other):
        return self.name.__lt__(other.name)

    def __le__(self, other):
        return self.name.__le__(other.name)

    def __eq__(self, other):
        return self.name.__eq__(other.name)

    def __ne__(self, other):
        return self.name.__ne__(other.name)

    def __gt__(self, other):
        return self.name.__gt__(other.name)

    def __ge__(self, other):
        return self.name.__ge__(other.name)


class BinaryOperationNode(ExpressionNode):
    def __init__(self, op: BinOp, arg1: ExpressionNode, arg2: ExpressionNode, **props) -> None:
        super().__init__(**props)
        self.op = op
        self.arg1 = arg1
        self.arg2 = arg2

    def __str__(self) -> str:
        return f"({self.arg1} {self.op.value} {self.arg2})"

    def visit(self, visitor):
        visitor.accept(self)
        self.arg1.visit(visitor)
        self.arg2.visit(visitor)

    def is_literal(self):
        return self.arg1.is_literal() and self.arg2.is_literal()

    def is__zero_literal(self):
        if self.op == BinOp.ADD or self.op == BinOp.SUB:
            return self.arg1.is__zero_literal() and self.arg2.is__zero_literal()
        elif self.op == BinOp.MUL or self.op == BinOp.DIV:
            return self.arg1.is__zero_literal() or self.arg2.is__zero_literal()

    @property
    def childs(self) -> Tuple[ExpressionNode, ExpressionNode]:
        return self.arg1, self.arg2


def normalize_binary_node(node: BinaryOperationNode):
    is_left_literal = node.arg1.is_literal()
    is_right_literal = node.arg2.is_literal()

    if is_right_literal and not is_left_literal:
        node.arg1, node.arg2 = node.arg2, node.arg1
        # e1 / a -> (1 / a) * e1
        if node.op == BinOp.DIV:
            node.arg1 = BinaryOperationNode(BinOp.DIV, LiteralNode('1'), node.arg1)
            node.op = BinOp.MUL


EMPTY_LITERAL = LiteralNode(None)
MINUS_ONE = LiteralNode("-1")
ZERO_LITERAL = LiteralNode("0")
