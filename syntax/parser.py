import inspect
import re
from collections import defaultdict
from contextlib import suppress

import pyparsing as pp
from varname import nameof

from .ast_tree import *
from .ast_tree import ExpressionNode

b = BinOp.ADD


class AlgebraLeeParser:
    def __init__(self):
        self.rules = defaultdict(list)
        self.start_rule = None
        self._init_rules()
        self._set_parser_action()

    def _register_rule_as(self, name, rule):
        self.rules[name].append(rule)

    def _init_rules(self):
        LPAR, RPAR = pp.Literal('(').suppress(), pp.Literal(')').suppress()
        ADD, SUB = pp.Literal('+'), pp.Literal('-')
        MUL, DIV = pp.Literal('*'), pp.Literal('/')

        number = pp.Regex('[+-]?\\d+\\.?\\d*')

        literal = number | pp.Word(re.sub(r'[eE]', '', pp.alphas))

        self._register_rule_as(nameof(literal), literal)

        identifier = pp.Regex('e\\d+')
        self._register_rule_as(nameof(identifier), identifier)

        expr = pp.Forward()

        group = literal | identifier | pp.Group(LPAR + expr + RPAR)

        mul = group + pp.ZeroOrMore((MUL | DIV) + group)
        mul.setName('binary_operation')
        self._register_rule_as('binary_operation', mul)

        add = mul + pp.ZeroOrMore((ADD | SUB) + mul)
        add.setName('binary_operation')
        self._register_rule_as('binary_operation', add)

        expr << add
        self.start_rule = expr

    def _set_parser_action(self):
        for rule_name, rules in self.rules.items():
            [self.set_parse_action(rule_name, rule) for rule in rules]

    def get_parser(self):
        return self.start_rule

    def parse_string(self, prog) -> ExpressionNode:
        locs = []
        row, col = 0, 0
        for ch in prog:
            if ch == '\n':
                row += 1
                col = 0
            elif ch == '\r':
                pass
            else:
                col += 1
            locs.append((row, col))
        old_init_action = AstNode.init_action

        def init_action(node: AstNode) -> None:
            loc = getattr(node, 'loc', None)
            if isinstance(loc, int):
                node.row = locs[loc][0] + 1
                node.col = locs[loc][1] + 1

        AstNode.init_action = init_action
        try:
            prog: ExpressionNode = self.get_parser().parseString(str(prog))[0]
            prog.program = True
            return prog
        finally:
            AstNode.init_action = old_init_action


    @staticmethod
    def set_parse_action(rule_name: str, parser: pp.ParserElement) -> None:
        if rule_name == rule_name.upper():
            return
        if rule_name == 'binary_operation':
            def bin_op_parse_action(s, loc, tocs):
                node = tocs[0]
                if not isinstance(node, AstNode):
                    node = bin_op_parse_action(s, loc, node)
                for i in range(1, len(tocs) - 1, 2):
                    second_node = tocs[i + 1]
                    if not isinstance(second_node, AstNode):
                        second_node = bin_op_parse_action(s, loc, second_node)
                    node = BinaryOperationNode(BinOp(tocs[i]), node, second_node)
                return node

            parser.setParseAction(bin_op_parse_action)
        else:
            cls = ''.join(x.capitalize() for x in rule_name.split('_')) + 'Node'
            with suppress(NameError):
                cls = eval(cls)

                if not inspect.isabstract(cls):
                    def parse_action(s, loc, tocs):
                        return cls(*tocs, loc=loc)

                    parser.setParseAction(parse_action)


global_parser = AlgebraLeeParser()
